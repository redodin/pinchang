﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebSipicha.Models;

namespace WebSipicha.Controllers
{
    public class PickingsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Pickings
        public async Task<ActionResult> Index()
        {
            var pickingSet = db.PickingSet.Include(p => p.Tea);
            return View(await pickingSet.ToListAsync());
        }

        // GET: Pickings/Details/5
        [HttpGet]
        public async Task<ActionResult> Details(int? id = 1)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Переделать после ввода системы авторизации
            Picking picking = await db.PickingSet
                .Include(pic => pic.CardSet)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (picking.CardSet.FirstOrDefault(c => c.Person == "Я") == null)
            {
                db.CardSet.Add(new Card { Person = "Я", PickingId = id });
                await db.SaveChangesAsync();
            }

            ViewBag.AromaLeafSet = db.AromaLeafSet;
            ViewBag.InfusionSet = db.InfusionSet;
            ViewBag.AromaInfusionSet = db.AromaInfusionSet;
            ViewBag.TasteInfusionSet = db.TasteInfusionSet;
            ViewBag.AfterTasteSet = db.AfterTasteSet;
            ViewBag.ImpressionSet = db.ImpressionSet;

            if (picking == null)
            {
                return HttpNotFound();
            }
            return View(picking);
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "addCardItem", MatchFormValue = "Добавить")]
        public async Task<ActionResult> Details(string Describe)
        {
            return PartialView("CheckBox");
        }

        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Сохранить")]
        public async Task<ActionResult> Details(int id)
        {
            Picking picking = await db.PickingSet
                .Include(pic => pic.CardSet)
                .FirstOrDefaultAsync(p => p.Id == id);

            var card = new Card() { Person = "Я", PickingId = id };

            foreach (var item in Request.Form)
            {
                if (!item.ToString().Contains("."))
                {
                    continue;
                }
                var keys = item.ToString().Split('.');
                int.TryParse(keys.Last(), out int keyId);
                switch (keys.First())
                {
                    case "AromaLeaf":
                        var AromaLeaf = await db.AromaLeafSet.FindAsync(keyId);
                        card.AromaLeafSet.Add(AromaLeaf);
                        break;
                    case "Infusion":
                        var infusion = await db.InfusionSet.FindAsync(keyId);
                        card.InfusionSet.Add(infusion);
                        break;
                    case "AromaInfusion":
                        var aromainfusion = await db.AromaInfusionSet.FindAsync(keyId);
                        card.AromaInfusionSet.Add(aromainfusion);
                        break;
                    case "TasteInfusion":
                        var tasteinfusion = await db.TasteInfusionSet.FindAsync(keyId);
                        card.TasteInfusionSet.Add(tasteinfusion);
                        break;
                    case "AfterTaste":
                        var aftertaste = await db.AfterTasteSet.FindAsync(keyId);
                        card.AfterTasteSet.Add(aftertaste);
                        break;
                    case "Impression":
                        var impression = await db.ImpressionSet.FindAsync(keyId);
                        card.ImpressionSet.Add(impression);
                        break;
                }
            }

            picking.CardSet.Remove(picking.CardSet.FirstOrDefault(c => c.Person == "Я"));
            picking.CardSet.Add(card);

            db.Entry(picking).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        // GET: Pickings/Create
        public ActionResult Create()
        {
            ViewBag.Season = new SelectList(new string[] { "Весна", "Лето", "Осень", "Зима" });
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name");
            return View();
        }

        // POST: Pickings/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Province,Year,Season,TeaId")] Picking picking)
        {
            if (ModelState.IsValid)
            {
                db.PickingSet.Add(picking);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name", picking.TeaId);
            return View(picking);
        }

        // GET: Pickings/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picking picking = await db.PickingSet.FindAsync(id);
            if (picking == null)
            {
                return HttpNotFound();
            }
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name", picking.TeaId);
            return View(picking);
        }

        // POST: Pickings/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Province,Year,Season,TeaId")] Picking picking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(picking).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name", picking.TeaId);
            return View(picking);
        }

        // GET: Pickings/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picking picking = await db.PickingSet.Include(p => p.PurchaseSet).FirstOrDefaultAsync(p => p.Id == id);
            if (picking == null)
            {
                return HttpNotFound();
            }
            return View(picking);
        }

        // POST: Pickings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Picking picking = await db.PickingSet.FindAsync(id);
            db.PickingSet.Remove(picking);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
