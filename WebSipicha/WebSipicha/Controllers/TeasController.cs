﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebSipicha.Models;

namespace WebSipicha.Controllers
{
    public class TeasController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Teas
        public async Task<ActionResult> Index()
        {
            return View(await db.TeaSet.ToListAsync());
        }

        // GET: Teas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tea tea = await db.TeaSet.FindAsync(id);
            if (tea == null)
            {
                return HttpNotFound();
            }
            return View(tea);
        }

        public ActionResult TeaInput()
        {
            return PartialView();
        }

        // GET: Teas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Teas/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Category")] Tea tea)
        {
            if (ModelState.IsValid)
            {
                db.TeaSet.Add(tea);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tea);
        }

        // GET: Teas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tea tea = await db.TeaSet.FindAsync(id);
            if (tea == null)
            {
                return HttpNotFound();
            }
            return View(tea);
        }

        // POST: Teas/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Category")] Tea tea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tea).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tea);
        }

        // GET: Teas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tea tea = await db.TeaSet.Include(p => p.PickingSet).FirstOrDefaultAsync(p => p.Id == id);
            if (tea == null)
            {
                return HttpNotFound();
            }
            return View(tea);
        }

        // POST: Teas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Tea tea = await db.TeaSet.FindAsync(id);
            db.TeaSet.Remove(tea);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
