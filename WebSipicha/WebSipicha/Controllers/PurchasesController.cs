﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebSipicha.Models;

namespace WebSipicha.Controllers
{

    public class PurchasesController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Purchases
        public async Task<ActionResult> Index()
        {
            var purchaseSet = db.PurchaseSet.Include(p => p.Shop);
            return View(await purchaseSet.ToListAsync());
        }

        // GET: Purchases/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Purchase purchase = await db.PurchaseSet.Include(p => p.Shop).Where(p => p.Id == id).FirstOrDefaultAsync();
            if (purchase == null)
            {
                return HttpNotFound();
            }
            return View(purchase);
        }

        // GET: Purchases/Create
        public ActionResult Create()
        {
            ViewBag.ShopId = new SelectList(db.ShopSet.Where(s => !s.IsClosed), "Id", "Name");
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name");
            ViewBag.Season = new SelectList(new string[] { "Весна", "Лето", "Осень", "Зима" });
            return View();
        }

        // POST: Purchases/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Готово")]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,Cost,ShopId")] Purchase purchase,
            [Bind(Include = "Id,Province,Season,Year,TeaId")]Picking picking)
        {
            if (purchase.Cost == null)
            {
                purchase.Cost = 0;
            }
            if (ModelState.IsValid)
            {
                var fromBasePick = db.PickingSet.Where(p => p.TeaId == picking.TeaId)
                    .FirstOrDefault(p => p.Province == picking.Province && p.Season == picking.Season && p.Year == picking.Year);
                int? fromBasePickingId = null;
                if (fromBasePick == null)
                {
                    db.PickingSet.Add(picking);
                    await db.SaveChangesAsync();
                }
                else
                {
                    fromBasePickingId = fromBasePick.Id;
                }

                purchase.Person = "Пользователь";
                purchase.PickingId = fromBasePickingId == null ? picking.Id : fromBasePickingId;
                db.PurchaseSet.Add(purchase);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ShopId = new SelectList(db.ShopSet.Where(s => !s.IsClosed), "Id", "Name");
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name");
            ViewBag.Season = new SelectList(new string[] { "Весна", "Лето", "Осень", "Зима" });
            return View(purchase);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultiButton(MatchFormKey = "shopAction", MatchFormValue = "Сохранить")]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,Name,Address,WebSite")] Shop shop,
            [Bind(Include = "Cost")] Purchase purchase,
            [Bind(Include = "Province,Season,Year,TeaId")]Picking picking)
        {
            if (ModelState.IsValid)
            {
                db.ShopSet.Add(shop);
                await db.SaveChangesAsync();
            }
            ViewBag.ShopId = new SelectList(db.ShopSet.Where(s => !s.IsClosed), "Id", "Name", shop.Id);
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name", picking.TeaId);
            ViewBag.Season = new SelectList(new string[] { "Весна", "Лето", "Осень", "Зима" }, picking.Season);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultiButton(MatchFormKey = "teaAction", MatchFormValue = "Сохранить")]
        public async Task<ActionResult> Create(
            [Bind(Include = "Id,Name,Category")] Tea tea,
            [Bind(Include = "Cost,ShopId")] Purchase purchase,
            [Bind(Include = "Province,Season,Year")]Picking picking)
        {
            if (ModelState.IsValid)
            {
                db.TeaSet.Add(tea);
                await db.SaveChangesAsync();
            }
            ViewBag.ShopId = new SelectList(db.ShopSet.Where(s => !s.IsClosed), "Id", "Name", purchase.ShopId);
            ViewBag.TeaId = new SelectList(db.TeaSet, "Id", "Name", tea.Id);
            ViewBag.Season = new SelectList(new string[] { "Весна", "Лето", "Осень", "Зима" }, picking.Season);
            return View();
        }

        // GET: Purchases/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Purchase purchase = await db.PurchaseSet.FindAsync(id);
            if (purchase == null)
            {
                return HttpNotFound();
            }
            ViewBag.PickingId = new SelectList(db.PickingSet, "Id", "Tea", purchase.PickingId);
            ViewBag.ShopId = new SelectList(db.ShopSet, "Id", "Name", purchase.ShopId);
            return View(purchase);
        }

        // POST: Purchases/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Person,Cost,ShopId,PickingId")] Purchase purchase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchase).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PickingId = new SelectList(db.PickingSet, "Id", "Tea", purchase.PickingId);
            ViewBag.ShopId = new SelectList(db.ShopSet, "Id", "Name", purchase.ShopId);
            return View(purchase);
        }

        // GET: Purchases/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Purchase purchase = await db.PurchaseSet.Include(p => p.Shop).Where(p => p.Id == id).FirstOrDefaultAsync();
            if (purchase == null)
            {
                return HttpNotFound();
            }
            return View(purchase);
        }

        // POST: Purchases/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Purchase purchase = await db.PurchaseSet.FindAsync(id);
            db.PurchaseSet.Remove(purchase);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
