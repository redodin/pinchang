﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebSipicha.Models;

namespace WebSipicha.Controllers
{
    public class ShopsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Shops
        public async Task<ActionResult> Index()
        {
            return View(await db.ShopSet.ToListAsync());
        }

        // GET: Shops/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = await db.ShopSet.FindAsync(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        public ActionResult ShopInput()
        {
            return PartialView();
        }

        // GET: Shops/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Shops/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Address,WebSite")] Shop shop)
        {
            if (ModelState.IsValid)
            {
                db.ShopSet.Add(shop);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(shop);
        }

        // GET: Shops/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = await db.ShopSet.FindAsync(id);
            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // POST: Shops/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Address,WebSite")] Shop shop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shop).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(shop);
        }

        // GET: Shops/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop shop = await db.ShopSet.Include(s => s.PurchaseSet).FirstOrDefaultAsync(s => s.Id == id);

            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        // POST: Shops/Delete/5
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Удалить")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Shop shop = await db.ShopSet.FindAsync(id);
            db.ShopSet.Remove(shop);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // POST: Shops/Delete/5
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Архивировать")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ArchiveConfirmed(int id)
        {
            Shop shop = await db.ShopSet.FindAsync(id);
            shop.IsClosed = true;
            db.Entry(shop).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // POST: Shops/Delete/5
        [HttpPost]
        [MultiButton(MatchFormKey = "action", MatchFormValue = "Разархивировать")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UnArchiveConfirmed(int id)
        {
            Shop shop = await db.ShopSet.FindAsync(id);
            shop.IsClosed = false;
            db.Entry(shop).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
