﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSipicha.Models
{
    public class Shop
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [StringLength(50)]
        [Display(Name = "Магазин")]
        public string Name { get; set; }

        [StringLength(80)]
        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [StringLength(50)]
        [Display(Name = "Веб-сайт")]
        public string WebSite { get; set; }

        [HiddenInput(DisplayValue = false)]
        [DefaultValue(false)]
        [Display(Name = "[Закрыто]")]
        public bool IsClosed { get; set; }

        public ICollection<Purchase> PurchaseSet { get; set; }
        public Shop()
        {
            PurchaseSet = new List<Purchase>();
        }
    }
}