﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSipicha.Models
{
    public class Card
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        public string Person { get; set; }

        public int? PickingId { get; set; }
        [Display(Name = "Сбор чая")]
        public Picking Picking { get; set; }

        [Display(Name = "Аромат сухого листа")]
        public virtual ICollection<AromaLeaf> AromaLeafSet { get; set; }
        [Display(Name = "Настой")]
        public virtual ICollection<Infusion> InfusionSet { get; set; }
        [Display(Name = "Аромат настоя")]
        public virtual ICollection<AromaInfusion> AromaInfusionSet { get; set; }
        [Display(Name = "Вкус настоя")]
        public virtual ICollection<TasteInfusion> TasteInfusionSet { get; set; }
        [Display(Name = "Послевкусие")]
        public virtual ICollection<AfterTaste> AfterTasteSet { get; set; }
        [Display(Name = "Впечатление")]
        public virtual ICollection<Impression> ImpressionSet { get; set; }

        public Card()
        {
            AromaLeafSet = new List<AromaLeaf>();
            InfusionSet = new List<Infusion>();
            AromaInfusionSet = new List<AromaInfusion>();
            TasteInfusionSet = new List<TasteInfusion>();
            AfterTasteSet = new List<AfterTaste>();
            ImpressionSet = new List<Impression>();
        }
    }

    public interface ICardItem
    {
        int Id { get; set; }
        string Describe { get; set; }
    }


    public class AromaLeaf : ICardItem
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Аромат сухого листа")]
        public string Describe { get; set; }

        public virtual ICollection<Card> Card { get; set; }
    }

    public class Infusion : ICardItem
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Настой")]
        public string Describe { get; set; }

        public virtual ICollection<Card> Card { get; set; }
    }

    public class AromaInfusion : ICardItem
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Аромат настоя")]
        public string Describe { get; set; }

        public virtual ICollection<Card> Card { get; set; }
    }

    public class TasteInfusion : ICardItem
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Вкус настоя")]
        public string Describe { get; set; }

        public virtual ICollection<Card> Card { get; set; }
    }

    public class AfterTaste : ICardItem
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Послевкусие")]
        public string Describe { get; set; }

        public virtual ICollection<Card> Card { get; set; }
    }

    public class Impression : ICardItem
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        [Display(Name = "Впечатление")]
        public string Describe { get; set; }

        public virtual ICollection<Card> Card { get; set; }
    }
}