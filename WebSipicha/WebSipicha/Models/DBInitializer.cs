﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace WebSipicha.Models
{
    public class DBInitializer : DropCreateDatabaseAlways<ModelContext>
    {
        protected override void Seed(ModelContext context)
        {
            context.ShopSet.AddRange(new List<Shop>()
            {
                new Shop() { Name = "Чайная студия", Address = "7-я лин. В.О., 34" },
                new Shop() { Name = "Хороший чай", Address = "Садовая ул., 35" }
            });
            context.SaveChanges();

            context.TeaSet.AddRange(new List<Tea>()
            {
                new Tea() { Name = "Мир в душе", Category = "Шу пуэр" },
                new Tea() { Name = "Мао Сё", Category = "Светлый улун" },
                new Tea() { Name = "Благородство", Category = "Шу пуэр" },
                new Tea() { Name = "Да Хун Пао", Category = "Тёмный улун" },
            });
            context.SaveChanges();

            context.AromaLeafSet.AddRange(new List<AromaLeaf>()
            {
                new AromaLeaf() { Describe = "Фруктовый"},
                new AromaLeaf() { Describe = "Дымный"},
                new AromaLeaf() { Describe = "Древесный"},
                new AromaLeaf() { Describe = "Ягодный"},
                new AromaLeaf() { Describe = "Сливочный"},
                new AromaLeaf() { Describe = "Ореховый"},
                new AromaLeaf() { Describe = "Медовый"},
                new AromaLeaf() { Describe = "Хлебный"}
            });

            context.InfusionSet.AddRange(new List<Infusion>()
            {
                new Infusion() { Describe = "Яркий"},
                new Infusion() { Describe = "Мутный"},
                new Infusion() { Describe = "Тусклый"},
                new Infusion() { Describe = "Прозрачный"}
            });

            context.AromaInfusionSet.AddRange(new List<AromaInfusion>()
            {
                new AromaInfusion() { Describe = "Цветочный"},
                new AromaInfusion() { Describe = "Копчённый"},
                new AromaInfusion() { Describe = "Ягодный"},
                new AromaInfusion() { Describe = "Сливочный"},
                new AromaInfusion() { Describe = "Медовый"}
            });

            context.TasteInfusionSet.AddRange(new List<TasteInfusion>()
            {
                new TasteInfusion() { Describe = "Сладкий"},
                new TasteInfusion() { Describe = "Маслянистый"},
                new TasteInfusion() { Describe = "Горький"},
                new TasteInfusion() { Describe = "Кислый"},
                new TasteInfusion() { Describe = "Сливочный"},
                new TasteInfusion() { Describe = "Терпкий"},
                new TasteInfusion() { Describe = "Медовый"},
                new TasteInfusion() { Describe = "Хлебный"}
            });

            context.AfterTasteSet.AddRange(new List<AfterTaste>()
            {
                new AfterTaste() { Describe = "Долгое"},
                new AfterTaste() { Describe = "Свежее"},
                new AfterTaste() { Describe = "Выраженное"},
                new AfterTaste() { Describe = "Вяжущее"}
            });

            context.ImpressionSet.AddRange(new List<Impression>()
            {
                new Impression() { Describe = "Рядовой"},
                new Impression() { Describe = "Бодрит"},
                new Impression() { Describe = "Любопытный"},
                new Impression() { Describe = "Согревает"},
                new Impression() { Describe = "Исключительный"},
                new Impression() { Describe = "Расслабляет"}
            });
            context.SaveChanges();

            context.PickingSet.AddRange(new List<Picking>()
            {
                new Picking
                {
                    TeaId = context.TeaSet.Find(1).Id,
                    Province = "Анси",
                    Year = 2009,
                    Season = "Весна",

                    CardSet = context.CardSet.Where(p => p.Person == "Я").ToList()
                },
                new Picking
                {
                    TeaId = context.TeaSet.Find(2).Id,
                    Province = "Анси",
                    Year = 2010,
                    Season = "Весна"
                },
                new Picking
                {
                    TeaId = context.TeaSet.Find(2).Id,
                    Province = "Анси",
                    Year = 2011,
                    Season = "Осень"
                }
            });
            context.SaveChanges();

            context.CardSet.AddRange(new List<Card>()
            {
                new Card
                {
                    Person = "Я",
                    PickingId = context.PickingSet.Find(1).Id,

                    AromaLeafSet = context.AromaLeafSet.Where(d => d.Describe.Length <= 7).ToList(),
                    InfusionSet = context.InfusionSet.Where(d => d.Describe.Length <= 7).ToList(),
                    TasteInfusionSet = context.TasteInfusionSet.Where(d => d.Describe.Length <= 7).ToList(),
                    AromaInfusionSet = context.AromaInfusionSet.Where(d => d.Describe.Length <= 7).ToList(),
                    AfterTasteSet = context.AfterTasteSet.Where(d => d.Describe.Length <= 7).ToList(),
                    ImpressionSet = context.ImpressionSet.Where(d => d.Describe.Length <= 7).ToList()
                },

                new Card
                {
                    Person = "Он",
                    PickingId = context.PickingSet.Find(1).Id,

                    AromaLeafSet = context.AromaLeafSet.Where(d => d.Describe.Length <= 5).ToList(),
                    InfusionSet = context.InfusionSet.Where(d => d.Describe.Length <= 9).ToList(),
                    TasteInfusionSet = context.TasteInfusionSet.Where(d => d.Describe.Length <= 0).ToList(),
                    AromaInfusionSet = context.AromaInfusionSet.Where(d => d.Describe.Length <= 4).ToList(),
                    AfterTasteSet = context.AfterTasteSet.Where(d => d.Describe.Length <= 3).ToList(),
                    ImpressionSet = context.ImpressionSet.Where(d => d.Describe.Length <= 10).ToList()
                },

                new Card
                {
                    Person = "Я",
                    PickingId = context.PickingSet.Find(2).Id,

                    AromaLeafSet = context.AromaLeafSet.Where(d => d.Describe.Length <= 6).ToList(),
                    InfusionSet = context.InfusionSet.Where(d => d.Describe.Length <= 5).ToList(),
                    TasteInfusionSet = context.TasteInfusionSet.Where(d => d.Describe.Length <= 1).ToList(),
                    AromaInfusionSet = context.AromaInfusionSet.Where(d => d.Describe.Length <= 2).ToList(),
                    AfterTasteSet = context.AfterTasteSet.Where(d => d.Describe.Length <= 6).ToList(),
                    ImpressionSet = context.ImpressionSet.Where(d => d.Describe.Length <= 7).ToList()
                },
            });
            context.SaveChanges();

            context.PurchaseSet.AddRange(new List<Purchase>()
            {
                new Purchase()
                {
                    Person = "Я",
                    Cost = 3.59M,
                    ShopId = context.ShopSet.Find(1).Id,
                    PickingId = context.PickingSet.Find(1).Id
                }
            });
            context.SaveChanges();

            base.Seed(context);
        }
    }
}