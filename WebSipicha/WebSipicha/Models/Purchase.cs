﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebSipicha.Models
{
    public class Purchase
    {
        //Атрибуты
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        public string Person { get; set; }

        [Display(Name = "Цена за грамм")]
        [DataType(DataType.Currency)]
        public decimal? Cost { get; set; }

        //Внешний ключ
        public int? ShopId { get; set; }

        [Display(Name = "Магазин")]
        public Shop Shop { get; set; }

        public int? PickingId { get; set; }

        [Display(Name = "Сбор чая")]
        public virtual Picking Picking { get; set; }
    }
}