﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebSipicha.Models
{
    public class ModelContext : DbContext
    {
        public DbSet<Card>          CardSet          { get; set; }
        public DbSet<Tea>           TeaSet           { get; set; }
        public DbSet<Picking>       PickingSet       { get; set; }
        public DbSet<AromaLeaf>     AromaLeafSet     { get; set; }
        public DbSet<Infusion>      InfusionSet      { get; set; }
        public DbSet<AromaInfusion> AromaInfusionSet { get; set; }
        public DbSet<TasteInfusion> TasteInfusionSet { get; set; }
        public DbSet<AfterTaste>    AfterTasteSet    { get; set; }
        public DbSet<Impression>    ImpressionSet    { get; set; }
        public DbSet<Shop>          ShopSet          { get; set; }
        public DbSet<Purchase>      PurchaseSet      { get; set; }

        public ModelContext() : base("DefaultConnection") { }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shop>()
                .HasMany(s => s.PurchaseSet)
                .WithRequired(p => p.Shop)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Picking>()
                .HasMany(pur => pur.PurchaseSet)
                .WithRequired(pic => pic.Picking)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Tea>()
                .HasMany(p => p.PickingSet)
                .WithRequired(t => t.Tea)
                .WillCascadeOnDelete(true);
        }
    }
}