﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSipicha.Models
{
    public class Tea
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [StringLength(50)]
        [Display(Name = "Чай")]
        public string Name { get; set; }

        [StringLength(50)]
        [Display(Name = "Категория")]
        public string Category { get; set; }

        public ICollection<Picking> PickingSet { get; set; }
        public Tea()
        {
            PickingSet = new List<Picking>();
        }
        public override string ToString()
        {
            return Name;
        }
    }
}