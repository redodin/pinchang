﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSipicha.Models
{
    public class Picking
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Display(Name = "Провинция")]
        [StringLength(50)]
        public string Province { get; set; }

        [Display(Name = "Год")]
        //[RegularExpression("^[1-2][0-9]{3}$", ErrorMessage = "Некорректный год")]
        [Range(1950, 2017)]
        public int? Year { get; set; }

        [Display(Name = "Сезон")]
        public string Season { get; set; }

        [Display(Name = "Чай")]
        public int? TeaId { get; set; }

        [Display(Name = "Чай")]
        public virtual Tea Tea { get; set; }

        public virtual ICollection<Purchase> PurchaseSet { get; set; }


        [Display(Name = "Карточка вкуса")]
        public ICollection<Card> CardSet { get; set; }

        public Picking()
        {
            PurchaseSet = new HashSet<Purchase>();

            CardSet = new HashSet<Card>();
        }

        public override string ToString()
        {
            return $"{Tea}: {Season} {Year}, {Province}";
        }

        public IEnumerable<ICardItem> GetAromaLeafSetByPerson(string name)
        {
            return GetCardByPerson(name).AromaLeafSet;
        }

        public IEnumerable<ICardItem> GetInfusionSetByPerson(string name)
        {
            return GetCardByPerson(name).InfusionSet;
        }

        public IEnumerable<ICardItem> GetAromaInfusionSetByPerson(string name)
        {
            return GetCardByPerson(name).AromaInfusionSet;
        }

        public IEnumerable<ICardItem> GetTasteInfusionSetByPerson(string name)
        {
            return GetCardByPerson(name).TasteInfusionSet;
        }

        public IEnumerable<ICardItem> GetAfterTasteSetByPerson(string name)
        {
            return GetCardByPerson(name).AfterTasteSet;
        }

        public IEnumerable<ICardItem> GetImpressionSetByPerson(string name)
        {
            return GetCardByPerson(name).ImpressionSet;
        }

        private Card GetCardByPerson(string name)
        {
            return CardSet.FirstOrDefault(c => c.Person == name);
        }
    }
}